#环境变量    
    SERVER-NAME：        运行CGI序为机器名或IP地址。
    SERVER-INTERFACE：   WWW服务器的类型，如：CERN型或NCSA型。
    SERVER-PROTOCOL：    通信协议，应当是HTTP/1.0。
    SERVER-PORT：        TCP端口，一般说来web端口是80。
    HTTP-ACCEPT：        HTTP定义的浏览器能够接受的数据类型。
    HTTP-REFERER：       发送表单的文件URL。（并非所有的浏览器都传送这一变量）
    HTTP-USER-AGENT：    发送表单的浏览器的有关信息。
    GETWAY-INTERFACE：   CGI程序的版本，在UNIX下为 CGI/1.1。
    PATH-TRANSLATED：    PATH-INFO中包含的实际路径名。
    PATH-INFO：          浏览器用GET方式发送数据时的附加路径。
    SCRIPT-NAME：        CGI程序的路径名。
    QUERY-STRING：       表单输入的数据，URL中间号后的内容。
    REMOTE-HOST：        发送程序的主机名，不能确定该值。
    REMOTE-ADDR：        发送程序的机器的IP地址。
    REMOTE-USER：        发送程序的人名。
    CONTENT-TYPE：       POST发送，一般为applioation/xwww-form-urlencoded。
    CONTENT-LENGTH：     POST方法输入的数据的字节数。

#遇到的错误
    502 Bad Gateway
    The CGI was not CGI/1.1 compliant.

    cgi_header：unable to find LFLF
